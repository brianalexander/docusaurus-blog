---
id: personal
title: This Site
---

Placeholder for setting up a home server using a Raspberry Pi, Nginx, Cloudflare, and Git.
