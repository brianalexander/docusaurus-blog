---
id: resume
title: Resume in Markdown
sidebar_label: Resume
slug: /
---

## Employment

---

<table>
  <tbody>
    <tr>
      <td><center><b>Software Developer</b></center></td>
      <td><center><b>InfoSend, Inc.</b></center></td>
      <td><center><b>February 2020 – Present</b></center></td>
    </tr>
    <tr>
      <td  colspan="3">
      • Developed, tested, deployed, and maintained programs for clients<br />
      • Worked with other teams to develop and refine project requirements<br />
      • Performed regression testing of in-house software tools before rollout
      </td>
    </tr>
  </tbody>
</table>

## Education

---

<table>
  <tbody>
    <tr>
      <td><b>Fullerton, CA</b></td>
      <td><center><b>CSU, Fullerton</b></center></td>
      <td><center><b>Spring 2021</b></center></td>
    </tr>
    <tr>
      <td  colspan="3">
      • M.S., May 2021. GPA: 4.0
      </td>
    </tr>
        <tr>
    <td><b>Riverside, CA</b></td>
      <td><center><b>UC, Riverside</b></center></td>
      <td><center><b>Winter 2010</b></center></td>
    </tr>
    <tr>
      <td  colspan="3">
      • B.A., March 2021. GPA: 3.08
      </td>
    </tr>
  </tbody>
</table>

## Projects and Extracurriculars

---

- **[Human-In-the-Loop Content Moderation System – Masters Capstone Project (2021)](/projects/capstone-project)**

  - Natural Language Processing models were trained using PyTorch to classify text into different categories such
    as offensive, hate speech, or toxic. Content that failed to meet a threshold of confidence are passed to human
    moderators who can flag or release the content through a consensus mechanism required a certain number
    of human votes
  - Kubernetes was used to handle microservice management and deployment
  - Redis was used as a message queue and as a cache for the database
  - Elasticsearch was used for querying of text-based documents and vector cosine similarity calculations.
  - The front-end was developed in React and used Redux for state management

- **Peer-to-Peer Distributed File Storage – Samsung 5G Hackathon (2019)**

  - Use WebRTC to facilitate Peer-to-Peer file storage over a mobile network
  - A Node.js server keeps track of users who are available to store files, as well as acting as a signaling server
  - When a user wants to retrieve a specific file, the app reaches out to the Node.js server, where it can look up
    the IDs of users who have the other pieces and get their connection information for initiating a P2P transfer

- **433Mhz Long Range Communication System – CSUF Titan Rover (2019)**
  - As lead of the communications team for CSUF Titan Rover 2019 I was responsible for developing and
    implementing the communication protocol between the base station and rover
  - Using an attached 433Mhz transceiver on each end, a python script streamed joystick commands from the
    base station to the rover and received GPS updates from the rover to the base station.
  - Automatic reconnection was implemented using exponential backoff in case the rover went out of range

## Hard and Soft Skills

---

<table>
<tbody>
  <tr>
    <td>Soft Skills</td>
    <td colspan="3">Effective Writing (Documentation); Communication (Teaching); Problem Solving; Leadership;</td>
  </tr>
  <tr>
    <td>Languages</td>
    <td colspan="3">Java; JavaScript; Python; SQL;</td>
  </tr>
  <tr>
    <td>Frameworks</td>
    <td colspan="3">Express; Flask; Pandas; PyTorch; React; Redux;</td>
  </tr>
  <tr>
    <td>Technologies</td>
    <td colspan="3">Docker; Elasticsearch; Git; Kubernetes; Node; PostgreSQL; Redis;</td>
  </tr>
</tbody>
</table>
