module.exports = {
  someSidebar: {
    Home: ["home/resume"],
    Projects: ["projects/capstone-project"],
    CodeWars: [
      {
        Python: ["codewars/doc3", "codewars/doc1", "codewars/doc2"],
      },
    ],
  },
};
