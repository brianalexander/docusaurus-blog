module.exports = {
  title: "Brian Alexander",
  tagline: "Mess with the zest, fry like the rest.",
  url: "https://brianalexander.netlify.app",
  baseUrl: "/",
  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",
  favicon: "img/favicon.ico",
  organizationName: "brianalexander", // Usually your GitHub org/user name.
  projectName: "docusaurus", // Usually your repo name.
  themeConfig: {
    gtag: {
      trackingID: "G-QX2BW8TLXG",
      anonymizeIP: true,
    },
    colorMode: {
      defaultMode: "light",
    },
    navbar: {
      title: "Brian Alexander",
      logo: {
        alt: "Site Logo",
        src: "img/logo.svg",
      },
      items: [
        { to: "blog/", label: "Blog", position: "left" },
        {
          label: "GitLab",
          href: "https://gitlab.com/brianalexander",
          position: "right",
        },
        {
          href: "https://github.com/brianalexander",
          label: "GitHub",
          position: "right",
        },
        {
          label: "LinkedIn",
          href: "https://www.linkedin.com/in/brianalexander28/",
          position: "right",
        },
      ],
    },
    footer: {
      style: "light",
      links: [
        {
          title: "Internal",
          items: [
            {
              label: "Home",
              to: "/",
            },
            {
              label: "Blog",
              to: "blog",
            },
          ],
        },
        {
          title: "Socials",
          items: [
            {
              label: "GitLab",
              href: "https://gitlab.com/brianalexander",
            },
            {
              label: "GitHub",
              href: "https://github.com/brianalexander",
            },
            {
              label: "LinkedIn",
              href: "https://www.linkedin.com/in/brianalexander28/",
            },
            {
              label: "Twitter",
              href: "https://twitter.com/brianalexand_r",
            },
          ],
        },
        {},
        {},
        {},
        {},
      ],
      copyright: `Brian Alexander © ${new Date().getFullYear()}<br />Origami Rabbit by <a href="https://www.flaticon.com/authors/freepik" target="_blank">Freepik</src>`,
    },
  },
  presets: [
    [
      "@docusaurus/preset-classic",
      {
        docs: {
          sidebarCollapsed: false,
          // path: "/",
          routeBasePath: "/",
          sidebarPath: require.resolve("./sidebars.js"),
          // Please change this to your repo.
          // editUrl:
          //   "https://gitlab.com/brianalexander/docusaurus-blog/-/tree/master",
        },
        blog: {
          showReadingTime: true,
          path: "./blog",
          // Please change this to your repo.
          // editUrl:
          //   "https://gitlab.com/brianalexander/docusaurus-blog/-/tree/master",
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      },
    ],
  ],
};
