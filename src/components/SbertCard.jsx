import React, { useState } from "react";
import ScoreInput from "./ScoreInput";
import ScoreResult from "./ScoreResult";
import { dot, norm } from "mathjs";

const cosineSimilarity = (emb1, emb2) => {
  const dotProduct = dot(emb1, emb2);

  const norm1 = norm(emb1);
  const norm2 = norm(emb1);

  const result = dotProduct / (norm1 * norm2);
  return result;
};

const FunctionalComponent = ({ buttonText, p1, p2 }) => {
  const [embedding1, setEmbedding1] = useState(null);
  const [embedding2, setEmbedding2] = useState(null);

  const setEmbedding1Handler = (scores) => {
    console.log(scores);
    setEmbedding1(scores);
  };

  const setEmbedding2Handler = (scores) => {
    console.log(scores);
    setEmbedding2(scores);
  };

  let similarity = null;
  if (embedding1 && embedding2) {
    similarity = {
      similarity: cosineSimilarity(embedding1.embedding, embedding2.embedding),
    };
  }

  return (
    <div
      className={"card"}
      style={{
        width: "100%",
        marginBottom: "10px",
      }}
    >
      <div
        className={"card-body d-flex flex-column"}
        style={{ rowGap: "10px" }}
      >
        <ScoreInput
          setScoresHandler={setEmbedding1Handler}
          placeholder={p1}
          buttonText={embedding1 ? "⟳" : buttonText}
          url={"https://sbert.maru.chat/predict?text="}
        />
        <ScoreInput
          setScoresHandler={setEmbedding2Handler}
          placeholder={p2}
          buttonText={embedding2 ? "⟳" : buttonText}
          url={"https://sbert.maru.chat/predict?text="}
        />
        {similarity ? <ScoreResult scores={similarity} /> : null}
      </div>
    </div>
  );
};

export default FunctionalComponent;
