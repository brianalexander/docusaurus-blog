import React, { useState } from "react";
import ScoreInput from "./ScoreInput";
import ScoreResult from "./ScoreResult";

const FunctionalComponent = ({ buttonText, placeholder }) => {
  const [scores, setScores] = useState(null);

  const setScoreHandler = (scores) => {
    for (const key of Object.keys(scores.scores)) {
      scores["scores"][key] = parseFloat(scores["scores"][key]).toFixed(2);
    }
    setScores(scores.scores);
  };

  return (
    <div
      className={"card"}
      style={{
        width: "100%",
        marginBottom: "10px",
      }}
    >
      <div
        className={"card-body d-flex flex-column"}
        style={{ rowGap: "10px" }}
      >
        <ScoreInput
          setScoresHandler={setScoreHandler}
          placeholder={placeholder}
          buttonText={scores ? "⟳" : buttonText}
          url={"https://ml-api.maru.chat/predict?text="}
        />
        <ScoreResult scores={scores} />
      </div>
    </div>
  );
};

export default FunctionalComponent;
