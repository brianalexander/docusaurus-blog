import React from "react";

// import styles from "/src/pages/bootstrap.module.css";

const FunctionalComponent = ({ scores }) => {
  if (scores === null || scores === undefined) {
    return null;
  }

  return (
    <div className={"container p-0 m-0 "}>
      <div className={"row"}>
        {Object.keys(scores).map((key, index) => {
          return (
            <center
              style={{ width: "100%" }}
              key={index}
              className={"text-dark col-4"}
            >
              {key}:{scores[key]}
            </center>
          );
        })}
      </div>
    </div>
  );
};

export default FunctionalComponent;
