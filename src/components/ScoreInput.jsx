import React, { useState } from "react";

const FunctionalComponent = ({
  buttonText,
  placeholder,
  prompt = ">",
  setScoresHandler,
  url,
}) => {
  const [text, setText] = useState("");

  const query = () => {
    fetch(`${url}${text}`)
      .then((response) => response.json())
      .then((json) => {
        console.log(json);
        setScoresHandler(json);
      });
  };

  return (
    <div className={"p-0 m-0 "}>
      <div className={"input-group"}>
        <span className={"input-group-text"}>{prompt}</span>
        <input
          type={"text"}
          className={"form-control"}
          aria-label={"Amount (to the nearest dollar)"}
          placeholder={placeholder}
          value={text}
          onChange={(e) => setText(e.target.value.toLowerCase())}
        />
        <button
          onClick={query}
          className={"text-dark btn btn-outline-secondary"}
          type={"button"}
        >
          {buttonText}
        </button>
      </div>
    </div>
  );
};

export default FunctionalComponent;
