import React from "react";

function Root({ children }) {
  console.log(`
        You found it! Now you have to 
           ____                    _____                    _____                    _____                            _____                    _____          
         /\\    \\                  /\\    \\                  /\\    \\                  /\\    \\                          /\\    \\                  /\\    \\         
        /::\\____\\                /::\\    \\                /::\\    \\                /::\\    \\                        /::\\____\\                /::\\    \\        
       /:::/    /                \\:::\\    \\              /::::\\    \\              /::::\\    \\                      /::::|   |               /::::\\    \\       
      /:::/    /                  \\:::\\    \\            /::::::\\    \\            /::::::\\    \\                    /:::::|   |              /::::::\\    \\      
     /:::/    /                    \\:::\\    \\          /:::/\\:::\\    \\          /:::/\\:::\\    \\                  /::::::|   |             /:::/\\:::\\    \\     
    /:::/____/                      \\:::\\    \\        /:::/__\\:::\\    \\        /:::/__\\:::\\    \\                /:::/|::|   |            /:::/__\\:::\\    \\    
   /::::\\    \\                      /::::\\    \\      /::::\\   \\:::\\    \\      /::::\\   \\:::\\    \\              /:::/ |::|   |           /::::\\   \\:::\\    \\   
  /::::::\\    \\   _____    ____    /::::::\\    \\    /::::::\\   \\:::\\    \\    /::::::\\   \\:::\\    \\            /:::/  |::|___|______    /::::::\\   \\:::\\    \\  
 /:::/\\:::\\    \\ /\\    \\  /\\   \\  /:::/\\:::\\    \\  /:::/\\:::\\   \\:::\\____\\  /:::/\\:::\\   \\:::\\    \\          /:::/   |::::::::\\    \\  /:::/\\:::\\   \\:::\\    \\ 
/:::/  \\:::\\    /::\\____\\/::\\   \\/:::/  \\:::\\____\\/:::/  \\:::\\   \\:::|    |/:::/__\\:::\\   \\:::\\____\\        /:::/    |:::::::::\\____\\/:::/__\\:::\\   \\:::\\____\\
\\::/    \\:::\\  /:::/    /\\:::\\  /:::/    \\::/    /\\::/   |::::\\  /:::|____|\\:::\\   \\:::\\   \\::/    /        \\::/    / ~~~~~/:::/    /\\:::\\   \\:::\\   \\::/    /
 \\/____/ \\:::\\/:::/    /  \\:::\\/:::/    / \\/____/  \\/____|:::::\\/:::/    /  \\:::\\   \\:::\\   \\/____/          \\/____/      /:::/    /  \\:::\\   \\:::\\   \\/____/ 
          \\::::::/    /    \\::::::/    /                 |:::::::::/    /    \\:::\\   \\:::\\    \\                          /:::/    /    \\:::\\   \\:::\\    \\     
           \\::::/    /      \\::::/____/                  |::|\\::::/    /      \\:::\\   \\:::\\____\\                        /:::/    /      \\:::\\   \\:::\\____\\    
           /:::/    /        \\:::\\    \\                  |::| \\::/____/        \\:::\\   \\::/    /                       /:::/    /        \\:::\\   \\::/    /    
          /:::/    /          \\:::\\    \\                 |::|  ~|               \\:::\\   \\/____/                       /:::/    /          \\:::\\   \\/____/     
         /:::/    /            \\:::\\    \\                |::|   |                \\:::\\    \\                          /:::/    /            \\:::\\    \\         
        /:::/    /              \\:::\\____\\               \\::|   |                 \\:::\\____\\                        /:::/    /              \\:::\\____\\        
        \\::/    /                \\::/    /                \\:|   |                  \\::/    /                        \\::/    /                \\::/    /        
         \\/____/                  \\/____/                  \\|___|                   \\/____/                          \\/____/                  \\/____/         

         :D
         `);

  return <>{children}</>;
}
export default Root;
